# rtnetlab-os Docker images
> A collection of Docker images, courtesy of rtnetlab Open-Source initiative.


## symfony-cli

This image contains the Symfony CLI binary. It is very simple to use: 

```shell
# Create a new Symfony project named "my-project"
$ docker run --rm \
  -v $PWD:/app \
  registry.gitlab.com/rtnetlab-os/docker-images/symfony-cli:latest \
  new my-project
```

**Note**: the container is running as root, so the generated files will be owned by `root:root`. This is easily fixable with `chown`, e.g. `chown -R $USER: my-project`

- https://symfony.com/download
- https://github.com/symfony-cli/symfony-cli
